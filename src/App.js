
import {Routes, Route, BrowserRouter} from 'react-router-dom'
import About from './pages/About';
import Home from './pages/Home';
import Settings from './pages/Settings';

function App() {
  return (
    <>
      <BrowserRouter>
        <Routes>
          <Route path='/' element={<Home />} />
          <Route path='/about' element={<About />} />
          <Route path='/settings' element={<Settings />} />
      </Routes>
      </BrowserRouter>
    </>
  );
}

export default App;
